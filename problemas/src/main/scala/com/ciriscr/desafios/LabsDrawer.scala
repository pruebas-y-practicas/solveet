package com.ciriscr.desafios

/**
 * Created with IntelliJ IDEA.
 * User: londo
 * Date: 25/09/12
 * Time: 01:37 PM
 */

trait LabsDrawer {

  def dibujar(file: String) {
    val lns = read(file)
    println(lns.flatten.map(procesarLinea).mkString("\n"))
  }

  private def read(file: String) = {
    new Reader(file).read.map(_.split('!'))
  }

  private def procesarLinea(l: String) = {
    var antNumero = false      //para saber si el anterior era un numero
    var num = 0
    var t = ""
    for (i <- l) {
      if (!antNumero && i.isDigit)
        num = i.asDigit
      else {
        if (i.isDigit){
          num += i.asDigit
        }
        if (i.toString.matches("""[A-Zb*]""")){
          t += (if (i == 'b') ' ' else i).toString * num
        }
      }
      antNumero = i.isDigit
    }
    t
  }

}
