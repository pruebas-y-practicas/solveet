package com.ciriscr.desafios

/**
 * Created with IntelliJ IDEA.
 * User: londo
 * Date: 31/05/12
 * Time: 08:16 PM
 */

//código fuente; https://bitbucket.org/londo/solveet-desafios
trait ArbolNavidad {

  def generateTree(n: Int) = {
    val l = for (i <- 0 until n) yield {
      (List.fill[String](n-i-1)(" ") ::: List.fill[String](i*2+1)("*")).mkString
    }
    l.mkString("\n")
  }

}
