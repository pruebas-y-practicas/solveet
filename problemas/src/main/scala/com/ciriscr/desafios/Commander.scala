package com.ciriscr.desafios

import scala.collection.mutable.Map

/**
 * Created with IntelliJ IDEA.
 * User: londo
 * Date: 21/08/12
 * Time: 03:11 PM
 */

object Commander {

  val params = Map.empty[String, Any]

  def main(a: Array[String]) {
    val it = a.toIterator
    while (it.hasNext){
      val arg = it.next()
      arg match {
        case "-d" => params += ("Base de datos" -> it.next())
        case "-c" => params += ("Coleccion" -> it.next())
        case "-h" => params += ("Host" -> it.next())
        case "-p" => params += ("Puerto" -> it.next().toInt)
        case "-q" => params += ("quiet" -> true)
        case e @ _ => throw new Exception(e + " - Argumento no soportado.")
      }
    }

  }

}
