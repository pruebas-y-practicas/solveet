package com.ciriscr.desafios

import org.scalatest.FunSuite

/**
 * Created with IntelliJ IDEA.
 * User: londo
 * Date: 01/06/12
 * Time: 09:40 PM
 */

class TestSumaPositivos extends FunSuite with SumaPositivos {

  test("Suma de positivos") {
    assert(sumar("sumaPositivos.in") == 11)
  }

}
